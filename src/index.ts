import server from './server'

const port = process.env.PORT || 3000

server.listen(port, (err) => {
  return console.log(err ? err : `Servidor ejecutandose en el puerto ${port}`)
})