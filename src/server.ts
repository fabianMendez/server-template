import * as Express from 'express'

class App {
  public express;
  
  constructor() {
    this.express = Express()
    // Inicializar rutas
    this.mountRoutes()
  }

  private mountRoutes(): void {
    const router = Express.Router()
    
    // Rutas
    router.get('/', (req, resp) => resp.json({code: 200}))

    this.express.use('/', router)
  }
}

export default new App().express